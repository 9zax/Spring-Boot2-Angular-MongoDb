import { Component, OnInit } from '@angular/core';
import { LocationStrategy, PlatformLocation, Location } from '@angular/common';
import { LegendItem, ChartType } from '../lbd/lbd-chart/lbd-chart.component';
import * as Chartist from 'chartist';
import { ACCESS_TOKEN } from '../const/val.service';
import { Router } from "@angular/router";
import { getContent, updateContent } from '../util/apiutils.service'
import { updateProfile } from '../util/apiutils.service'
export const API_BASE_URL = 'http://localhost:5000/api';

const swal = require('sweetalert2')


@Component({
  selector: 'app-content',
  templateUrl: './content.component.html',
  styleUrls: ['./content.component.css']
})
export class ContentComponent implements OnInit {
  public emailChartType: ChartType;
  public emailChartData: any;
  public emailChartLegendItems: LegendItem[];

  public hoursChartType: ChartType;
  public hoursChartData: any;
  public hoursChartOptions: any;
  public hoursChartResponsive: any[];
  public hoursChartLegendItems: LegendItem[];

  public activityChartType: ChartType;
  public activityChartData: any;
  public activityChartOptions: any;
  public activityChartResponsive: any[];
  public activityChartLegendItems: LegendItem[];

  constructor(private router: Router) { }




  content1 = {
    name: "",
    description: ""
  }

  content2 = {
    name: "",
    description: ""
  }

  statuslogin:Boolean=false;

  ngOnInit() {

    this.SetContent();

  }


  SetContent() {
    if (localStorage.getItem(ACCESS_TOKEN) == null || localStorage.getItem(ACCESS_TOKEN) == 'undefined') {
      this.router.navigate(['/login']);
      this.statuslogin=false;
    }
    else {
      this.statuslogin=true;
      console.log(localStorage.getItem(ACCESS_TOKEN));
    }

    getContent(1).then(response => {
      this.content1.name = response["name"];
      this.content1.description = response["description"];
    });


    getContent(2).then(response => {
      this.content2.name = response["name"];
      this.content2.description = response["description"];
    });
  }


  fUpdateContent(gid, type) {

    const modelcontent = {
      id: gid,
      name: "",
      description: ""

    }

    if (type == 'name') {
      var val = "";
      if (gid == 1) {
        val = this.content1.name;
        modelcontent.description = this.content1.description;
      }
      else {
        val = this.content2.name;
        modelcontent.description = this.content2.description;
      }

      swal({
        title: 'ใส่ชื่อเนื้อหา',
        input: 'textarea',
        allowOutsideClick: false,
        inputValue: val
      }).then(function (text) {
        if (text) {
          modelcontent.name = text["value"];
          console.log(modelcontent);

          updateContent(modelcontent)
            .then(response => {
              swal({
                position: 'top-end',
                type: 'error',
                title: 'ผิดพลาดในการบันทึกข้อมูล',
                showConfirmButton: false,
                timer: 1500
              })

            }).catch(error => {
              swal({
                position: 'top-end',
                type: 'success',
                title: 'อัพเดทข้อมูลสำเร็จ',
                showConfirmButton: false,
                timer: 1500
              })

              setTimeout(() => 
              {
                window.location.reload();

              },
              3000);
              

            });



        }
      })
    }

    else {
      var val = "";
      if (gid == 1) {
        val = this.content1.description;
        modelcontent.name = this.content1.name;
      }
      else {
        val = this.content2.description;
        modelcontent.name = this.content2.name;
      }
      swal({
        title: 'ใส่เนื้อหา',
        input: 'textarea',
        allowOutsideClick: false,
        inputValue: val
      }).then(function (text) {
        if (text) {
          modelcontent.description = text["value"];
          console.log(modelcontent);

          updateContent(modelcontent)
            .then(response => {
              swal({
                position: 'top-end',
                type: 'error',
                title: 'ผิดพลาดในการบันทึกข้อมูล',
                showConfirmButton: false,
                timer: 1500
              })

            }).catch(error => {
              swal({
                position: 'top-end',
                type: 'success',
                title: 'อัพเดทข้อมูลสำเร็จ',
                showConfirmButton: false,
                timer: 1500
              })

              setTimeout(() => 
              {
                window.location.reload();

              },
              3000);
              
            });



        }
      })
    }



  }


}
