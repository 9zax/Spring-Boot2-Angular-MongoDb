import { Component, OnInit } from '@angular/core';
import { ACCESS_TOKEN,ACCESS_USERNAME } from '../const/val.service';
import {Router} from "@angular/router";
import {getCurrentUser} from '../util/apiutils.service'
import {AllfunctionService} from '../allfunction.service'
import {login} from '../util/apiutils.service'
import { updateProfile } from '../util/apiutils.service'

export const API_BASE_URL = 'http://localhost:5000/api';

import {LoginComponent} from '../login/login.component'

const swal = require('sweetalert2')

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.css']
})
export class UserComponent implements OnInit {

  constructor(private router:Router,private getuser:AllfunctionService) { }

  iduser:String;
  name:String;
  username:String;
  

  ngOnInit() {


   

    if(localStorage.getItem(ACCESS_TOKEN)==null || localStorage.getItem(ACCESS_TOKEN)=='undefined'){
      this.router.navigate(['/login']);
    }



     
      getCurrentUser().then(response => {
        
         console.log(response);

         this.iduser=response["id"];

         this.getuser.SetUser(response["name"],response["username"]);

         this.name=this.getuser.modelUser["name"];

         this.username=this.getuser.modelUser["username"];

        });
        

    
  }


  UpdateProfile(na, us, ps){
    
    const signupRequest = {
        name: na,
        username: us,
        password: ps
    };


    localStorage.setItem(ACCESS_USERNAME, us);

    updateProfile(signupRequest,this.iduser)
    .then(response => {
        if(response["message"]!=undefined){
          alert(response["message"]);
        }
        else{

          getCurrentUser().then(response => {
    
            this.getuser.SetUser(response["name"],response["username"]);
    
            this.name=this.getuser.modelUser["name"];
    
            this.username=this.getuser.modelUser["username"];
    
            });

            swal({
              position: 'top-end',
              type: 'success',
              title: 'อัพเดทข้อมูลสำเร็จ',
              showConfirmButton: false,
              timer: 1500
          })

          this.router.navigate(['/user']);

          
          
        }
        
        console.log(response);
        
    }).catch(error => {
        swal({
              position: 'top-end',
              type: 'error',
              title: 'กรอกข้อมูลให้ครบถ้วน',
              showConfirmButton: false,
              timer: 1500
          })
    });
  }

}
