import { NgModule } from '@angular/core';
import { CommonModule, } from '@angular/common';
import { BrowserModule  } from '@angular/platform-browser';
import { Routes, RouterModule } from '@angular/router';

import { ContentComponent } from './content/content.component';
import { UserComponent } from './user/user.component';
import { IconsComponent } from './icons/icons.component';
import { LoginComponent } from './login/login.component';
import { HomeComponent } from './home/home.component';


const routes: Routes =[
    { path: 'content',        component: ContentComponent },
    { path: 'user',           component: UserComponent },
    { path: 'setting',        component: IconsComponent },
    { path: 'login',          component: LoginComponent },
    { path: 'home',           component: HomeComponent },
    { path: '',               redirectTo: 'home', pathMatch: 'full' }
];

@NgModule({
  imports: [
    CommonModule,
    BrowserModule,
    RouterModule.forRoot(routes)
  ],
  exports: [
  ],
})
export class AppRoutingModule { }
