import { Injectable } from '@angular/core';
import { Router } from "@angular/router";
import { ACCESS_TOKEN,ACCESS_USERNAME } from './const/val.service';

@Injectable()
export class AllfunctionService {

  constructor(private router: Router) {
    

   }

  modelUser = {
    name: "",
    username: ""
  };

  Logout() {
    localStorage.removeItem(ACCESS_TOKEN);
    localStorage.removeItem(ACCESS_USERNAME);
    this.router.navigate(['/login']);
  }

  SetUser(na,us) {
    this.modelUser.name=na;
    this.modelUser.username=us;
  }

}

