package com.example.springapi.model;


import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

import javax.validation.constraints.NotBlank;


@Document(collection = "content_collection")
public class Content {

	@Indexed
	private Integer id;

	@NotBlank
	private String name;

	@NotBlank
	private String description;
	
	public Content(){ }
	
	public Content(Integer idv, String name, String description)
	{
		this.id =idv;
		this.name = name;
		this.description = description;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
	 
	
}
